# git-diff.yaml

Get differences from the git repository:

## Job .changelog

*Input Variables:*

* CHANGELOG_FROM: reference to the commit to compare to. Defaults to the tag before the current tag (sorted by date).
* CHANGELOG_TO: reference to the commit to compare. Defaults to the current CI tag.
* CHANGELOG_FORMAT: Format for printing the chagelog. See [git-log](https://git-scm.com/docs/git-log) for details
* CHANGELOG_FILE: The file to create for the changelog. Existing files will be overridden. Can be saved in an artifact or evaluated in `after-script`. Defaults to `CHANGELOG`

*Output Variables:*

None

*Description:* Creates a changelog between two references.

## Job .changed_files

*Input Variables:*

* CHANGES_FROM: reference to the commit to compare to. Defaults to the tag before the current tag (sorted by date).
* CHANGES_TO: reference to the commit to compare. Defaults to the current CI tag.
* CHANGES_DIFF_FILE: The file to write the changed files to. Existing files will be overridden. Can be saved in an artifact or evaluated in `after-script`. Defaults to `CHANGED_FILES`

*Output Variables:*

None

*Description:* Gets all files changed between to references.
