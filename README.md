# CI/CD helpers provided by Carsten

This is a small set of helpers for the Gitlab CI/CD.

## Usage

In your `.gitlab-ci.yml` do the following:

```YAML
include:
  - project: 'https://gitlab.com/carstencodes/ci'  
    ref: 'v1.0.0'
    file: 'ci.yml'

my-job:
  stage: prepare-build
  extends: .my-job
  variables:
    - OVERRIDE: YOUR
    - VARIABLES: HERE
```

The following files are available:

## git-diff.yaml

A set of helpers for managing differences between commits, tags or - in general - git refs. See [git-diff.md](git-diff.md) for more information.

## License

These files are licensed under MIT license.
